<?php

/**
 * Implementation of hook_install().
 */
function uwd_membership_install() {
  drupal_install_schema('uwd_membership');
}

/**
 * Implementation of hook_uninstall().
 */
function uwd_membership_uninstall() {
  drupal_uninstall_schema('uwd_membership');
}

/**
 * Implementation of hook_schema().
 */
function uwd_membership_schema() {
  $schema['uwd_membership_types'] = array(
    'description' => 'The base table for memberships.', 
    'fields' => array(
      'mtid' => array(
        'description' => 'The primary identifier for a membership type.', 
        'type' => 'serial', 
        'unsigned' => TRUE, 
        'not null' => TRUE,
      ), 
      'nid' => array(
        'description' => 'The product nid to check in complete orders.', 
        'type' => 'int', 
        'unsigned' => TRUE, 
        'not null' => TRUE,
        'default' => 0,
      ), 
      'time' => array(
        'description' => 'The time to add when the product is added.', 
        'type' => 'int', 
        'unsigned' => TRUE, 
        'not null' => TRUE, 
        'default' => 0,
      ), 
      'rid' => array(
        'description' => 'The role id to add/remove with this membership.', 
        'type' => 'int', 
        'unsigned' => TRUE, 
        'not null' => TRUE,
        'default' => 0,
      ), 
    ), 
    'unique keys' => array(
      'nid_rid' => array('nid', 'rid'), 
    ), 
    'primary key' => array('mtid'),
  );
  $schema['uwd_membership_users'] = array(
    'description' => 'The table for user memberships.', 
    'fields' => array(
      'mid' => array(
        'description' => 'The primary identifier for a membership.', 
        'type' => 'serial', 
        'unsigned' => TRUE, 
        'not null' => TRUE,
      ), 
      'uid' => array(
        'description' => 'The user id.', 
        'type' => 'int', 
        'unsigned' => TRUE, 
        'not null' => TRUE,
        'default' => 0,
      ), 
      'rid' => array(
        'description' => 'The role id to add/remove with this membership.', 
        'type' => 'int', 
        'unsigned' => TRUE, 
        'not null' => TRUE,
        'default' => 0,
      ), 
      'time' => array(
        'description' => 'The timestamp when the membership expires.', 
        'type' => 'int', 
        'unsigned' => TRUE, 
        'not null' => TRUE, 
        'default' => 0,
      ), 
      'orders' => array(
        'description' => 'The orders that have modified this role membership.', 
        'type' => 'varchar', 
        'length' => 1000,
        'not null' => TRUE, 
        'default' => '',
        'serialize' => TRUE,
      ), 
      'expired' => array(
        'description' => 'If the membership is expired (1) or not (0).', 
        'type' => 'int',
        'size' => 'tiny',
        'not null' => TRUE, 
        'default' => 0,
      ), 
    ), 
    'unique keys' => array(
      'uid_rid' => array('uid', 'rid'), 
    ), 
    'primary key' => array('mid'),
  );
  return $schema;
}