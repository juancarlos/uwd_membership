<?php

function uwd_membership_user_list() {
  $output = '<i>Pending to be implemented</i>';
  
  return $output;
}

function uwd_membership_type_list() {
  $header = array(
    t('Product'),
    t('Role'),
    t('Time'),
    t('Cart Link'),
  );
  $query = "SELECT umt.* FROM {uwd_membership_types} umt ORDER BY umt.time DESC";
  $result = db_query($query);
  $rows = array();
  while ($row = db_fetch_object($result)) {
    $node = node_load($row->nid);
    $roles = user_roles(TRUE);
    $url = 'cart/add/p' . $row->nid . '_q1';
    $rows[] = array(
      $node->title,
      $roles[$row->rid],
      $row->time,
      l($url . '?destination=cart/checkout', $url, array('query' => array('destination' => 'cart/checkout'))),
    );
  }
  $output .= theme_table($header, $rows);  
  return $output;
}

function uwd_membership_types_add($form_state) {
  $query = "SELECT n.nid, n.title FROM {uc_products} up, {node} n WHERE up.nid = n.nid ORDER BY up.ordering, n.title";
  $result = db_query($query);
  $options = array();
  while ($node = db_fetch_object($result)) {
    $options[$node->nid] = $node->title;
  }
  $form['nid'] = array(
    '#title' => t('Product'),
    '#type' => 'select',
    '#options' => $options,
    '#required' => TRUE,
  );
  $options = drupal_map_assoc(range(1, 48));
  $form['time-qty'] = array(
    '#title' => 'Time',
    '#type' => 'select',
    '#options' => $options,
    '#required' => TRUE,
  );
  $options = array(
    'h' => t('Hours'),
    'd' => t('Days'),
    'm' => t('Months'),
    'y' => t('Years'),
  );
  $form['time-unit'] = array(
    '#type' => 'select',
    '#options' => $options,
    '#required' => TRUE,
  );
  $form['rid'] = array(
    '#title' => t('Role'),
    '#type' => 'select',
    '#options' => user_roles(TRUE),
    '#required' => TRUE,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Add'),
  );
  $form['#redirect'] = 'admin/store/memberships/types/list';
  return $form;
}

function uwd_membership_types_add_validate($form, &$form_state) {
  $query = "SELECT COUNT(*) FROM {uwd_membership_types} umt WHERE umt.nid='%d' AND umt.rid='%d'";
  $count = db_result(db_query($query, $form_state['values']['nid'], $form_state['values']['rid']));
  if ($count) {
    form_set_error('', t('That combination of Node and Role already have a membership created.'));
  }
}

function uwd_membership_types_add_submit($form, &$form_state) {
  $nid = $form_state['values']['nid'];
  $rid = $form_state['values']['rid'];
  $time = $form_state['values']['time-qty'];
  switch ($form_state['values']['time-unit']) {
    case 'y':
      $time *= 60*60*24*365.25;
      break;
    case 'm':
      $time *= 60*60*24*31;
      break;
    case 'd':
      $time *= 60*60*24;
      break;
    case 'h':
      $time *= 60*60;
      break;
  }
  $object = (object) array(
    'nid' => $nid,
    'rid' => $rid,
    'time' => $time,
  );
  if (drupal_write_record('uwd_membership_types', $object)) {
    drupal_set_message(t('The new membership type was added.'));
  }
  else {
    drupal_set_message(t('Something went wrong when trying to add the new membership.'), 'error');
  }
}